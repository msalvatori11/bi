import { SolicitacaoListComponent } from './solicitacao-list/solicitacao-list.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolicitacaoFormComponent } from './solicitacao-form/solicitacao-form.component';

const routes: Routes = [
  { path: '', component: SolicitacaoListComponent},
  { path: 'new', component: SolicitacaoFormComponent},
  { path: ':id/edit', component: SolicitacaoFormComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitacoesRoutingModule { }
