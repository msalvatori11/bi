import { CustoService } from './../../custos/shared/custo.service';
import { SelectItem } from 'primeng/api';
import { Validators } from '@angular/forms';
import { Cargo } from './../../cargos/shared/cargo.model';
import { Pessoa } from './../shared/pessoa.model';
import { Component, OnInit, Injector } from '@angular/core';
import { PessoaService } from '../shared/pessoa.service';
import { CargoService } from '../../cargos/shared/cargo.service';
import { Custo } from '../../custos/shared/custo.model';
import { BaseResourceForm } from 'src/app/shared/base-resource-form/base-resource-form.abstract';

@Component({

  selector: 'app-pessoa-form',
  templateUrl: './pessoa-form.component.html',
  styleUrls: ['./pessoa-form.component.css'],


})

export class PessoaFormComponent extends BaseResourceForm<Pessoa> {

  constructor(
    protected pessoaService: PessoaService,
    protected cargoService: CargoService,
    protected custoService: CustoService,
    protected injector: Injector) {
    super(injector, new Pessoa(), pessoaService, Pessoa.fromJson);
    this.loadCargos();
    this.loadCustos();
    this.tipoPessoas = [
      {label: 'Física', value: 'F'},
      {label: 'Jurídica', value: 'J'}
  ];
  }
  cargos: Array<Cargo>;
  custos: Array<Custo>;
  tipoPessoas: SelectItem[];
  compare = 'id';

 compareFn = this._compareFn.bind(this);

  protected buildResourceForm() {
   this.resourceForm = this.formBuilder.group({
      id: [null],
      descricao: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(150) ]],
      tipopessoa: [null, Validators.required],
      idcargo: [null],
      cliente: [null],
      empreiteiro: [null],
      comprador: [null],
      cargo: [],
      custo: []

    });
  }

 _compareFn(a, b) {
   if (a && b ) {
     return a[this.compare] === b[this.compare];
   }
 }


  protected creationPageTitle(): string {

    return 'Cadastro de Pessoas';
  }

  protected editionPageTitle(): string {
     return 'Editando: ';
  }


  private loadCargos() {

    this.cargoService.getAll().subscribe(
      cargos => this.cargos = cargos
    );
  }

  private loadCustos() {

    this.custoService.getAll().subscribe(
      custos => this.custos = custos
    );
  }
}

