import { Custo } from './../../custos/shared/custo.model';
import { Cargo } from './../../cargos/shared/cargo.model';
import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';
export class Pessoa extends BaseResourceModel {
  constructor(
    public id?:number,
    public tipopessoa?: string,
    public cliente?: boolean,
    public empreiteiro?: boolean,
    public comprador?: boolean,
    public cargo?:Cargo,
    public custo?:Custo

  ){
    super();
  }


  static fromJson(jsonData: any): Pessoa {
    return Object.assign(new Pessoa(), jsonData);
  }


}
