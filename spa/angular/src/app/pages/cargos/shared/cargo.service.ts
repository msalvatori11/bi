import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Cargo } from './cargo.model';
import { Injectable, Injector } from '@angular/core';
import { BaseResourceService } from "../../../shared/services/base-resource.service";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CargoService extends BaseResourceService<Cargo> {


  constructor(protected injector: Injector) {

    super("http://localhost:9090/cargos", injector, Cargo.fromJson);

  }
  private getAdministrativos(): Observable<Cargo[]> {
    return this.http.get(`http://localhost:9090/cargos/administrativos`).pipe(
      map(this.jsonDataToResources.bind(this)),
      catchError(this.handleError)
    )

  }

}
