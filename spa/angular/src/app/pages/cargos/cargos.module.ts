import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CargosRoutingModule } from './cargos-routing.module';
import { CargoFormComponent } from './cargo-form/cargo-form.component';
import { CargoListComponent } from './cargo-list/cargo-list.component';

@NgModule({
  imports: [
    CommonModule,
    CargosRoutingModule,
    SharedModule
  ],
  declarations: [CargoListComponent, CargoFormComponent]
})
export class CargosModule { }
