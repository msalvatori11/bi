import { Custo } from './../../custos/shared/custo.model';
import { Pessoa } from './../../pessoas/shared/pessoa.model';
import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';

export class Obra extends BaseResourceModel {

  constructor(

    public id?:number,
    public descricao?: string,
    public endereco?: string,
    public dataInc?: string,
    public dataFim?: string,
    public empreiteiro?: Pessoa,
    public comprador?: Pessoa,
    public administrativo?: Pessoa,
    public engenheiro?: Pessoa,
    public gerenteContrato?: Pessoa,
    public cliente?: Pessoa,
    public custo?: Custo,
    public ativo?: boolean

  ){
    super();
  }


  static fromJson(jsonData: any): Obra {
    return Object.assign(new Obra(), jsonData);
  }
}
