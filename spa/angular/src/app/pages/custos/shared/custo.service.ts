import { Injectable, Injector } from '@angular/core';
import { Custo } from "./custo.model";

import { BaseResourceService } from "../../../shared/services/base-resource.service";

@Injectable({
  providedIn: 'root'
})
export class CustoService extends BaseResourceService<Custo> {


  constructor(protected injector: Injector) {

    super("http://localhost:9090/custos", injector, Custo.fromJson);

  }

}
