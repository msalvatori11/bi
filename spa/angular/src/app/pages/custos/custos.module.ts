import { CustoFormComponent } from './custo-form/custo-form.component';
import { CustoListComponent } from './custo-list/custo-list.component';
import { NgModule } from '@angular/core';
import { SharedModule } from "../../shared/shared.module";
import { CustosRoutingModule } from './custos-routing.module';

@NgModule({
  imports: [
    SharedModule,
    CustosRoutingModule
  ],
  declarations: [CustoListComponent, CustoFormComponent]
})
export class CustosModule { }
