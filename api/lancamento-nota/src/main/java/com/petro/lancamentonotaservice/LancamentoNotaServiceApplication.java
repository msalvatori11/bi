package com.petro.lancamentonotaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LancamentoNotaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LancamentoNotaServiceApplication.class, args);
	}

}
