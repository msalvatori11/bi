package com.petro.lancamentonotaservice.model;


import com.petro.lancamentonotaservice.library.BaseBean;
import com.petro.lancamentonotaservice.library.DTOEntity;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="empresa")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@SequenceGenerator(name = "seq_empresa", sequenceName = "seq_empresa",  allocationSize = 10)
public class Empresa extends BaseBean<Long> implements Serializable, DTOEntity {


    private static final long serialVersionUID = -4962401885477778013L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_empresa")
    private Long id;

    @Length(min = 1, max = 200, message = "Nome Fantasia deve conter entre 1 e 200  caracteres")
    @Column(name = "nome_fantasia",nullable = false)
    private String nomeFantasia;


    @Length(min = 1, max = 200, message = "Razao Social deve conter entre 1 e 200  caracteres")
    @Column(name = "razao_social",nullable = false)
    private String razaoSocial;

    @Length(min = 1, max = 16, message = "Razao Social deve conter entre 1 e 16  caracteres")
    @Column(name = "cnpj",nullable = false, length = 16)
    private String cnpj;


    @Length(min = 1, max = 16, message = "Razao Social deve conter entre 1 e 100  caracteres")
    @Column(name = "nome_unidade")
    private String nomeUnidade;

    @Column(name = "cep")
    private String cep;

    @Length(min = 1, max = 200, message = "Endereco  deve conter entre 1 e 200  caracteres")
    @Column(name = "endereco")
    private String endereco;

    @Length(min = 1, max = 12, message = "Telefone1  deve conter entre 1 e 12  caracteres")
    @Column(name = "telefone1")
    private String telefone1;

    @Length(min = 1, max = 12, message = "Telefone2  deve conter entre 1 e 12  caracteres")
    @Column(name = "telefone2")
    private String telefone2;

    @Length(min = 1, max = 12, message = "Telefone Responsavel  deve conter entre 1 e 12  caracteres")
    @Column(name = "telefone_responsavel")
    private String telefone;

    @Length(min = 1, max = 12, message = "Responsavel  deve conter entre 1 e 200  caracteres")
    @Column(name = "responsavel")
    private String responsavel;

    @Length(min = 1, max = 12, message = "Cpf Responsavel  deve conter entre 1 e 16  caracteres")
    @Column(name = "cpf_responsavel")
    private String cpfResponsavel;

    @Length(min = 1, max = 200, message = "Email Responsavel  deve conter entre 1 e 200  caracteres")
    @Column(name = "email_responsavel")
    private String emailResponsavel;

    @Length(min = 1, max = 2, message = "Uf  deve conter entre 1 e 2  caracteres")
    @Column(name = "estado")
    private String estado;

    @Length(min = 1, max = 200, message = "Cidade  deve conter entre 1 e 200  caracteres")
    @Column(name = "cidade")
    private String cidade;

    @Override
    public Long getIdEntity() {
        return  this.id;
    }

}