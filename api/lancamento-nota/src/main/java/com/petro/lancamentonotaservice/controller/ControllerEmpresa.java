package com.petro.lancamentonotaservice.controller;

import com.petro.lancamentonotaservice.library.BaseController;
import com.petro.lancamentonotaservice.model.Empresa;
import com.petro.lancamentonotaservice.model.EmpresaDto;
import com.petro.lancamentonotaservice.service.EmpresaService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/empresas")
public class ControllerEmpresa extends BaseController<EmpresaDto, Empresa, EmpresaService, Long> {

    @PostConstruct
    public void ControllerEmpresa(){ super.BaseController(new Empresa(), new EmpresaDto());}
}
