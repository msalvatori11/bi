package com.petro.lancamentonotaservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.petro.lancamentonotaservice.library.DTOEntity;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Date;

@JsonPropertyOrder({"id"})
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ContratoDto extends RepresentationModel<ContratoDto> implements Serializable, DTOEntity {

    private static final long serialVersionUID = 6533682546987427333L;


    @JsonProperty("id")
    private Long id;

    @JsonProperty("nomeContrato")
    private String nomeContrato;

    @JsonProperty("descricao")
    private String descricao;

    @JsonProperty("nomeResponsavel")
    private String nomeResponsavel;

    @JsonProperty("emailResponsavel")
    private String emailResponsavel;

    @JsonProperty("telefoneResponsavel")
    private String telefoneResponsavel;

    @JsonProperty("dtExercicio")
    private Date dtExercicio;

    @JsonProperty("empresa")
    private EmpresaDto empresa;


}
